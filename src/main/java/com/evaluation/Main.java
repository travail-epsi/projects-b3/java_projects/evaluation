package com.evaluation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.consommation.Consommation;

public class Main {
  public static void main(String[] args) {
    String filePath = "assets/consommation.csv";
    try {
      List<Consommation> cs = returnConsommations(filePath);

      String outputString = "output/consommation.html";

      generateHTML(cs, outputString);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static List<Consommation> returnConsommations(String filePath) throws IOException {
    BufferedReader buffer = new BufferedReader(new FileReader(filePath));
    List<Consommation> cs = new ArrayList<>();
    while (buffer.ready()) {
      String line = buffer.readLine();
      String[] tab = line.split(";");
      if (tab[0].equals("Id"))
        continue;
      cs.add(new Consommation(tab[0], tab[1], tab[2], Integer.parseInt(tab[3])));
    }
    buffer.close();
    return cs;
  }

  private static void generateHTML(List<Consommation> cs, String outputString) throws FileNotFoundException {
    PrintWriter pWriter = new PrintWriter(outputString);
    pWriter.println(
        "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" crossorigin=\"anonymous\">");

    pWriter.println("<h1 class='text-center'> Guillaume Ros </h1>");
    pWriter.println("<div class=\"table-responsive\">");
    pWriter.println("<table class=\"table table-striped\">\n" +
        "<thead>" +
        "<tr>" +
        "<th class=\"table-light\">Id</th>" +
        "<th class=\"table-light\">Date</th>" +
        "<th class=\"table-light\">Consommateur</th>" +
        "<th class=\"table-light\">Nombre</th>" +
        "</tr>" +
        "</thead>");
    int somme = 0;
    for (Consommation c : cs) {
      pWriter.println("<tr>" +
          "<td>" + c.getId() + "</td>" +
          "<td>" + c.getDate() + "</td>" +
          "<td>" + c.getConsommateur() + "</td>" +
          "<td>" + c.getNombre() + "</td>" +
          "</tr>");
      somme += c.getNombre();
    }
    pWriter
        .println("<tfoot><tr>" + "<td></td><td></td><td>Somme</td>" + "<td>" + somme + "</td>" + "</tr></tfoot>");

    pWriter.println("</table>");
    pWriter.println("</div>");

    pWriter.close();
  }
}
