package com.consommation;

public class Consommation {
  private String id;
  private String date;
  private String consommateur;
  private int nombre;

  public Consommation(String id, String date, String consommateur, int nombre) {
    this.id = id;
    this.date = date;
    this.consommateur = consommateur;
    this.nombre = nombre;
  }

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDate() {
    return this.date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getConsommateur() {
    return this.consommateur;
  }

  public void setConsommateur(String consommateur) {
    this.consommateur = consommateur;
  }

  public int getNombre() {
    return this.nombre;
  }

  public void setNombre(int nombre) {
    this.nombre = nombre;
  }

  @Override
  public String toString() {
    return "{" +
      " id='" + getId() + "'" +
      ", date='" + getDate() + "'" +
      ", consommateur='" + getConsommateur() + "'" +
      ", nombre='" + getNombre() + "'" +
      "}";
  }

}
